//Khai báo express
const express = require('express');
//Khai báo port
const port = 8000;
//Tạo app
const app = express();
//Khai báo path
const path = require('path');
app.use(express.static(path.join(__dirname, 'views')))

app.use('/', (req, res, next) => {
    let today = new Date;
    console.log(`ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}, ${today.getHours()} giờ ${today.getMinutes()} phút`)
    console.log(`URL của request là : ${req.url}`)
    next()
})

//Liên kết với views/index
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'))
})


app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})